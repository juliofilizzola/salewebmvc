using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SaleWebMVC.Models.ViewModel;
using SaleWebMVC.Service;

namespace SaleWebMVC.Controllers;

public class HomeController(
    ILogger<HomeController> logger,
    DepartmentService       departmentService,
    SellerService           sellerService,
    SaleRecordService       saleRecordService,
    DepartmentsController   departmentsController,
    SaleRecordController    saleRecordController,
        SellerController sellerController
    )
    : Controller {
    private readonly ILogger<HomeController> _logger        = logger;

    public async Task<IActionResult> Index() {
        var departments = await departmentService.FindAll();
        var sellers     = await sellerService.FindAll();
        var saleRecords = await saleRecordService.FindAll();
        var viewModel = new HomeViewModel {
            Departments = departments,
            Sellers     = sellers,
            SaleRecords = saleRecords
        };
        return View(viewModel);
    }

    public IActionResult Privacy() {
        return View();
    }

    public async Task<IActionResult> EditDepartments(int? id) {
        return await departmentsController.Edit(id);
    }

    public async Task<IActionResult> DetailsDepartments(int? id) {
        return await departmentsController.Details(id);
    }

    public async Task<IActionResult> DeleteDepartments(int? id) {
        return await departmentsController.Delete(id);
    }

    public async Task<IActionResult> EditSaleRecord(int? id) {
        return await saleRecordController.Edit(id);
    }

    public async Task<IActionResult> DetailsSaleRecord(int? id) {
        return await saleRecordController.Details(id);
    }

    public async Task<IActionResult> DeleteSaleRecord(int? id) {
        return await saleRecordController.Delete(id);
    }

    public async Task<IActionResult> EditSeller(int? id) {
        return await sellerController.Edit(id);
    }

    public async Task<IActionResult> DetailsSeller(int? id) {
        return await sellerController.Details(id);
    }

    public async Task<IActionResult> DeleteSeller(int? id) {
        return await sellerController.Delete(id);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error() {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}